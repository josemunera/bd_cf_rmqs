# BD_CF_RMQS 

## Bulk density, coarse fragments and fine earth in topsoil and subsoil in France 



This repository has the R script used to extract and generate the national datasets of bulk density and coarse fragments in topsoils and subsoil using the data from the French Soil Monitoring Network (RMQS).

## Files

The datasets are provided in csv and rds formats.

