library(tidyverse)
library(dplyr)
library(RODBC)
library(infosolR)
# ##
##

Raw_data_rmqs_1 <- readRDS(file = "Data/Raw_data_rmqs_1.rds")

Raw_data_rmqs_2 <- readRDS(file = "Data/Raw_data_rmqs_2.rds")

Raw_data <- rbind(Raw_data_rmqs_1,Raw_data_rmqs_2) %>%
  as.data.frame()

#Raw_data <- readRDS(file = "Data/Raw_data.rds") 

source("R/weighted_average.r")

###

#### Bulk density and coarse fragments ####

##### Campagne 1 ####
#limites inferior et superior moyennes dans chaque site 

RMQS_1_volum <- Raw_data %>% filter(id_campaign == 1) %>%
  dplyr::select(id_campaign,id_site, Layer,upp_lim_sample,
         Low_lim_sample, Inf_limit_layer, Sup_limit_layer,
          BD, CF) %>%
  dplyr::rename(profBases =Low_lim_sample, 
         profSommets=upp_lim_sample, 
         limBasePrelev=Inf_limit_layer, 
         limSommetPrelev=Sup_limit_layer,  
         DAs=BD, 
         EGs=CF) %>%
  drop_na(profBases, profSommets, 
          limBasePrelev, limSommetPrelev,
          DAs#,EGs 
  ) %>% 
  dplyr::mutate(id_site_Layer = paste0(id_site, "_", Layer))



# Use the function


DA_calc <- list()
data_id_hor <- list()
for (i in  unique(RMQS_1_volum$id_site_Layer)) {
  
  
  data <- RMQS_1_volum %>% filter(id_site_Layer == i ) %>%
    dplyr::select(!c(#id_site,no_horizon, id_campagne,
      id_site_Layer)) %>%
    drop_na(profBases, profSommets, 
            limBasePrelev, limSommetPrelev,
            DAs#,EGs
    )
  
  DA_calc[[paste0(i)]] <-  ponderationMesuresVol(profBases =data$profBases,
                                                 profSommets = data$profSommets, 
                                                 limBasePrelev = data$limBasePrelev,
                                                 limSommetPrelev = data$limSommetPrelev, 
                                                 DAs = data$DAs, 
                                                 EGs = data$EGs)
  
  
  
  data_id_hor[[paste0(i)]] <- c(mean(data$id_site), mean(data$Layer))
  
}

df_DA_calc <- as.data.frame(do.call(rbind, DA_calc))  %>%
  dplyr::rename(BD_wm = V1,
         CF_wm = V2)

df_id_hor <- as.data.frame(do.call(rbind, data_id_hor)) %>%
  dplyr::rename(id_site = V1,
         Layer = V2)


Data_pond_Tot_1 <- cbind(df_id_hor,df_DA_calc)

##### Campagne 2 ####

RMQS_2_volum <-  Raw_data %>% filter(id_campaign == 2) %>%
  dplyr::select(id_campaign,id_site, Layer,upp_lim_sample,
         Low_lim_sample, Inf_limit_layer, Sup_limit_layer,
         BD, CF) %>%
  dplyr::rename(profBases =Low_lim_sample, 
         profSommets=upp_lim_sample, 
         limBasePrelev=Inf_limit_layer, 
         limSommetPrelev=Sup_limit_layer,  
         DAs=BD, 
         EGs=CF) %>%
  drop_na(profBases, profSommets, 
          limBasePrelev, limSommetPrelev,
          DAs#,EGs 
  ) %>% 
  dplyr::mutate(id_site_Layer = paste0(id_site, "_", Layer))

# Use the function


DA_calc <- list()
data_id_hor <- list()
for (i in  unique(RMQS_2_volum$id_site_Layer)) {
  
  
  data <- RMQS_2_volum %>% filter(id_site_Layer == i ) %>%
    dplyr::select(!c(#id_site,no_horizon, id_campagne,
      id_site_Layer)) %>%
    drop_na(profBases, profSommets, 
            limBasePrelev, limSommetPrelev,
            DAs#,EGs
    )
  
  DA_calc[[paste0(i)]] <-  ponderationMesuresVol(profBases =data$profBases,
                                                 profSommets = data$profSommets, 
                                                 limBasePrelev = data$limBasePrelev,
                                                 limSommetPrelev = data$limSommetPrelev, 
                                                 DAs = data$DAs, 
                                                 EGs = data$EGs)
  
  
  
  data_id_hor[[paste0(i)]] <- c(mean(data$id_site), mean(data$Layer))
  
}

df_DA_calc <- as.data.frame(do.call(rbind, DA_calc))  %>%
  dplyr::rename(BD_wm = V1,
         CF_wm = V2)

df_id_hor <- as.data.frame(do.call(rbind, data_id_hor)) %>%
  dplyr::rename(id_site = V1,
         Layer = V2)



Data_pond_Tot_2 <- cbind(df_id_hor,df_DA_calc)

#### Fine earth ####

##### Campagne 1 #####

RMQS_1_volum_dTF <- Raw_data %>% filter(id_campaign == 1) %>%
  dplyr::select(id_campaign,id_site, Layer,upp_lim_sample,
         Low_lim_sample, Inf_limit_layer, Sup_limit_layer,
         BD, CF, FE,Moist_cont) %>%
  dplyr::rename(profBases =Low_lim_sample, 
         profSommets=upp_lim_sample, 
         limBasePrelev=Inf_limit_layer, 
         limSommetPrelev=Sup_limit_layer,  
         DAs=FE, 
         EGs=Moist_cont) %>%
  drop_na(profBases, profSommets, 
          limBasePrelev, limSommetPrelev,
          DAs#,EGs 
  ) %>% 
  dplyr::mutate(id_site_Layer = paste0(id_site, "_", Layer))


# Use the function


DA_calc <- list()
data_id_hor <- list()
for (i in  unique(RMQS_1_volum_dTF$id_site_Layer)) {
  
  
  data <- RMQS_1_volum_dTF %>% filter(id_site_Layer == i ) %>%
    dplyr::select(!c(#id_site,no_horizon, id_campagne,
      id_site_Layer)) %>%
    drop_na(profBases, profSommets, 
            limBasePrelev, limSommetPrelev,
            DAs#,EGs
    )
  
  DA_calc[[paste0(i)]] <-  ponderationMesuresVol(profBases =data$profBases,
                                                 profSommets = data$profSommets, 
                                                 limBasePrelev = data$limBasePrelev,
                                                 limSommetPrelev = data$limSommetPrelev, 
                                                 DAs = data$DAs, 
                                                 EGs = data$EGs)
  
  
  
  data_id_hor[[paste0(i)]] <- c(mean(data$id_site), mean(data$Layer))
  
}

df_DA_calc <- as.data.frame(do.call(rbind, DA_calc))  %>%
  dplyr::rename(FE_wm = V1,
         Moist_wm = V2)

df_id_hor <- as.data.frame(do.call(rbind, data_id_hor)) %>%
  dplyr::rename(id_site = V1,
         Layer = V2)

Data_pond_Tot_1_dTF <- cbind(df_id_hor,df_DA_calc)

##### Campagne 2 #####

RMQS_2_volum_dTF <- Raw_data %>% filter(id_campaign == 2) %>%
  dplyr::select(id_campaign,id_site, Layer,upp_lim_sample,
                Low_lim_sample, Inf_limit_layer, Sup_limit_layer,
                BD, CF, FE,Moist_cont) %>%
  dplyr::rename(profBases =Low_lim_sample, 
                profSommets=upp_lim_sample, 
                limBasePrelev=Inf_limit_layer, 
                limSommetPrelev=Sup_limit_layer,  
                DAs=FE, 
                EGs=Moist_cont) %>%
  drop_na(profBases, profSommets, 
          limBasePrelev, limSommetPrelev,
          DAs#,EGs 
  ) %>% 
  dplyr::mutate(id_site_Layer = paste0(id_site, "_", Layer))


# Use the function


DA_calc <- list()
data_id_hor <- list()
for (i in  unique(RMQS_2_volum_dTF$id_site_Layer)) {
  
  
  data <- RMQS_2_volum_dTF %>% filter(id_site_Layer == i ) %>%
    dplyr::select(!c(#id_site,no_horizon, id_campagne,
      id_site_Layer)) %>%
    drop_na(profBases, profSommets, 
            limBasePrelev, limSommetPrelev,
            DAs#,EGs
    )
  
  DA_calc[[paste0(i)]] <-  ponderationMesuresVol(profBases =data$profBases,
                                                 profSommets = data$profSommets, 
                                                 limBasePrelev = data$limBasePrelev,
                                                 limSommetPrelev = data$limSommetPrelev, 
                                                 DAs = data$DAs, 
                                                 EGs = data$EGs)
  
  
  
  data_id_hor[[paste0(i)]] <- c(mean(data$id_site), mean(data$Layer))
  
}

df_DA_calc <- as.data.frame(do.call(rbind, DA_calc))  %>%
  dplyr::rename(FE_wm = V1,
                Moist_wm = V2)

df_id_hor <- as.data.frame(do.call(rbind, data_id_hor)) %>%
  dplyr::rename(id_site = V1,
                Layer = V2)

Data_pond_Tot_2_dTF <- cbind(df_id_hor,df_DA_calc)


#### Merge data ####

##### Weighted averages ####

Data_pond_1 <- 
  Data_pond_Tot_1 %>% left_join(Data_pond_Tot_1_dTF,
                                by = c("id_site", "Layer")) %>%
                     dplyr::mutate(CF_wm = ifelse(CF_wm== 0 & is.na(FE_wm)==T,
                                             NA, CF_wm),
                                   id_campaign = 1)



Data_pond_2 <- 
  Data_pond_Tot_2 %>% left_join(Data_pond_Tot_2_dTF, 
                                by = c("id_site", "Layer")) %>%
                      dplyr::mutate(CF_wm = ifelse(CF_wm== 0 & is.na(FE_wm)==T,
                                              NA, CF_wm),
                                   id_campaign = 2)



Data_pond_Tot <- bind_rows(Data_pond_1, Data_pond_2) %>%
                  mutate(id_campaign = as.numeric(id_campaign))

##### Other variables ####

Dataset_A <-
  Raw_data %>%  
  dplyr::select(id_campaign,id_cellule ,id_site, Layer,Land_use_code,Biosoil,da_no_methode,
         y_theo,x_theo,volume, sampling_date,
         Sup_limit_layer, Inf_limit_layer  ) %>%
  group_by(id_site, id_cellule, id_campaign,da_no_methode, Layer,Land_use_code,x_theo,y_theo ) %>%
  dplyr::summarise(Sup_limit_layer = mean(Sup_limit_layer, na.rm = T),
                   Inf_limit_layer = mean(Inf_limit_layer, na.rm = T),
                   sampling_date = mean(sampling_date, na.rm = T),
                   volume=mean(volume, na.rm=TRUE),
                   .groups = 'drop') %>%
  left_join(Data_pond_Tot, by = c("id_site",
                                  "Layer",
                                  "id_campaign")) %>%
  ungroup() %>%
  filter(!(id_site >3000 & id_site< 4000)) %>% #Include only sites in Mainland France
  filter(!id_site >13000)

#saveRDS(object = Dataset_A,
#file = "Data/Dataset_A.rds" )



#write.csv(Dataset_A,"Data/Dataset_A.csv", row.names = FALSE)
