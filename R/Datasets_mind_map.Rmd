---
title: "Soil physical parameters RMQS"
author: "Jose L. Munera-Echeverri"
date: "23/11/2023"
output: html_document
---


# Raw data

## RMQS_1 (DOI 1) & RMQS_2 (DOI 2)

### Methods

### Individual values

### Upper & lower depth of each sample

### Upper & lower depth of composite layer

### BD, CF, FE & moisture at sampling


# Weigthed means 

## Dataset A (DOI 3)

### Campaigns 1 & 2 

### Upper & lower depth of composite layer

### Mean values per composite layer

## Dataset B (DOI 4)

### Average of campaigns 1 & 2

### Values at 0-30cm & 30-50cm

## Weigthed average function 

